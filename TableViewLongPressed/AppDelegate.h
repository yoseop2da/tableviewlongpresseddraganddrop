//
//  AppDelegate.h
//  TableViewLongPressed
//
//  Created by yoseop on 2014. 10. 21..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

