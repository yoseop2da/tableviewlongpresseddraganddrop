//
//  UIAlertView+Block.h
//  TableViewLongPressed
//
//  Created by yoseop on 2014. 10. 21..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonItem.h"

@interface UIAlertView (Block)

-(id)initWithTitle:(NSString *)inTitle message:(NSString *)inMessage cancelButtonItem:(ButtonItem *)inCancelButtonItem otherButtonItems:(ButtonItem *)inOtherButtonItems, ... NS_REQUIRES_NIL_TERMINATION;

- (NSInteger)addButtonItem:(ButtonItem *)item;

@end