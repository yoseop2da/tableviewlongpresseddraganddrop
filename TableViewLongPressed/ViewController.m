//
//  ViewController.m
//  TableViewLongPressed
//
//  Created by yoseop on 2014. 10. 21..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "ViewController.h"
#import "UIAlertView+Block.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong) NSMutableArray *allDatas;
@property (strong) UIAlertView *addAlertView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"메인 타이틀";
    
    [self loadData];
    [self addLongPressGestureToTableView];
    
    [self addRightBarButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 기본 데이터 셋팅

- (void)loadData
{
    self.allDatas = [@[@"심장의",@"구할",@"청춘",@"피는",@"되려니와",@"하는",@"속잎나고",@"구하기",@"구하지",@"듣는다"] mutableCopy];
}

#pragma mark - 오른쪽 버튼 클릭

- (void)addRightBarButton
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(rightbarbuttonItemTouched)];
}

- (void)rightbarbuttonItemTouched
{
    ButtonItem *cancelButtonItem = [ButtonItem itemWithLabel:@"취소"];
    [cancelButtonItem setAction:^{}];
    
    ButtonItem *okButtonItem = [ButtonItem itemWithLabel:@"추가"];
    [okButtonItem setAction:^{
        UITextField *textField = [self.addAlertView textFieldAtIndex:0];
        NSString *string = [textField.text capitalizedString];
        [self.allDatas addObject:string];
        
        NSUInteger row = self.allDatas.count - 1;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    self.addAlertView = [[UIAlertView alloc] initWithTitle:@"아이템 추가" message:nil cancelButtonItem:cancelButtonItem otherButtonItems:okButtonItem, nil];
    self.addAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    self.addAlertView.tag = 1;
    [self.addAlertView show];
}



#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allDatas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.textLabel.text = self.allDatas[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d 번째 셀을 터치했음 [ %@ ]",indexPath.row, self.allDatas[indexPath.row]);
}

#pragma mark - LongPress Gesture

- (void)addLongPressGestureToTableView
{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureForRecognized:)];
    [self.tableView addGestureRecognizer:longPress];
}

- (void)longPressGestureForRecognized:(UILongPressGestureRecognizer *)longPress
{
    /*
     터치 상태 확인
     */
    UIGestureRecognizerState state = longPress.state;
    NSLog(@"state : %d",state); //1:터치시작 ,2:드래그중, 3:터치업
    
    /*
     드래그하고있는 현재 위치
     */
    CGPoint location = [longPress locationInView:self.tableView];
    NSIndexPath *draggedCellCurrentIndexPath = [self.tableView indexPathForRowAtPoint:location];
    NSLog(@"내가 드래그하고 있는 현재 테이블뷰의 row : %d",draggedCellCurrentIndexPath.row);
    
    /*
     드래그할 Cell의 그림자와, 초기IndexPath값 저장.
     */
    static UIView *snapShot = nil;
    static NSIndexPath *sourceCellIndexPath = nil;
    
    switch (state) {
        case UIGestureRecognizerStateBegan:
        {
            if (draggedCellCurrentIndexPath) {
                sourceCellIndexPath = draggedCellCurrentIndexPath;
            }
            //
            UITableViewCell *draggedCell = [self.tableView cellForRowAtIndexPath:draggedCellCurrentIndexPath];
            snapShot = [self cellSnapShotFromView:draggedCell];

            __block CGPoint center = draggedCell.center;
            snapShot.center = center;
            snapShot.alpha = 0.0;
            
            [self.tableView addSubview:snapShot];
            [UIView animateWithDuration:0.25f animations:^{
                
                center.y = location.y;
                snapShot.center = center;
                snapShot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                snapShot.alpha = 0.98;
                draggedCell.alpha = 0.0;

            } completion:^(BOOL finished) {
                
                draggedCell.hidden = YES;

            }];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGPoint center = snapShot.center;
            center.y = location.y;
            snapShot.center = center;
            if (draggedCellCurrentIndexPath && ![draggedCellCurrentIndexPath isEqual:sourceCellIndexPath]) {
                
                [self.allDatas exchangeObjectAtIndex:draggedCellCurrentIndexPath.row withObjectAtIndex:sourceCellIndexPath.row];
                
                [self.tableView moveRowAtIndexPath:sourceCellIndexPath toIndexPath:draggedCellCurrentIndexPath];
                
                sourceCellIndexPath = draggedCellCurrentIndexPath;
            }
        }
            break;
        default:
        {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:sourceCellIndexPath];
            cell.hidden = NO;
            cell.alpha = 0.0;
            
            [UIView animateWithDuration:0.25f animations:^{
                
                snapShot.center = cell.center;
                snapShot.transform = CGAffineTransformIdentity;
                snapShot.alpha = 0.0f;
                cell.alpha = 1.0f;
                
            } completion:^(BOOL finished) {
                
                sourceCellIndexPath = nil;
                [snapShot removeFromSuperview];
                snapShot = nil;
                
            }];
        }
            break;
    }
    
    
}

- (UIView *)cellSnapShotFromView:(UIView *)draggedCell
{
    UIView *cellShadow = [draggedCell snapshotViewAfterScreenUpdates:YES];
    cellShadow.layer.masksToBounds = YES;
    cellShadow.layer.cornerRadius = 0.0f;
    cellShadow.layer.shadowOffset = CGSizeMake(-0.5f, 0.0f);
    cellShadow.layer.shadowRadius = 5.0f;
    cellShadow.layer.shadowOpacity = 0.3;
    return cellShadow;
}

#pragma mark - 스와이프시 삭제 해주기.

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.allDatas removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}



@end
